package com.artivisi.training.microservice.gatewayscg.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletFallbackController {
    
    @GetMapping("/fallbackhostinfo")
    public Map<String, String> fallbackHostInfo(){
        Map<String, String> defaultHostInfo = new HashMap<>();
        defaultHostInfo.put("LocalPort", "0");
        defaultHostInfo.put("Hostname", "not.available");
        defaultHostInfo.put("Local IP", "0.0.0.0");
        return defaultHostInfo;
    }
}
