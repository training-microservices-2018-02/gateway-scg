package com.artivisi.training.microservice.gatewayscg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayScgApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayScgApplication.class, args);
	}
}
